var accessToken = '2305080156.5b9e1e6.44be3290f6634a2083dea04ce65d65ec';
var limit = 10;
var setSize = "low_resolution";
var contador = 0;

var instagramTag = function() {
    return {
        init: function() {
            instagramTag.loadImages();
        },
        loadImages: function() {
            var getImagesURL = 'https://api.instagram.com/v1/tags/osteoin/media/recent?client_id=b49727be751a4f769f43295ac079f906&access_token='+ accessToken +'';
            $.ajax({
                type: "GET",
                dataType: "jsonp",
                cache: false,
                url: getImagesURL,
                success: function(data) {
                    if(data.data.length) {
                        contador = data.data.length;
                        for (var i = 0; i < limit && contador > 0; i++) {
                            if (setSize == "small") {
                                var size = data.data[i].images.thumbnail.url;
                            } else if (setSize == "medium") {
                                var size = data.data[i].images.low_resolution.url;
                            } else {
                                var size = data.data[i].images.standard_resolution.url;
                            }
                            $("#portfolio").append("<div class='portfolio-item tagosteoin col-md-4 col-sm-6'><img src='" + size + "' alt='tagosteoin' /><a target='_blank' href='" + data.data[i].link + "'><img src='images/plus.png' alt='portfolio-arrow' /></a><div class='portfolio-item-hover'> <h3>" + data.data[i].caption.text + "</h3><h4>#osteoin</h4></div></div>");
                            contador -= 1;
                        }
                    }
                }
            });
        }
    }
}();

var instagramPerfil = function() {
    return {
        init: function() {
            instagramPerfil.loadImages();
        },
        loadImages: function() {
            var getImagesURL = 'https://api.instagram.com/v1/users/self/media/recent/?access_token='+ accessToken +'';
            $.ajax({
                type: "GET",
                dataType: "jsonp",
                cache: false,
                url: getImagesURL,
                success: function(data) {
                    if(data.data.length) {
                        contador = data.data.length;
                        for (var i = 0; i < limit && contador > 0; i++) {
                            if (setSize == "small") {
                                var size = data.data[i].images.thumbnail.url;
                            } else if (setSize == "medium") {
                                var size = data.data[i].images.low_resolution.url;
                            } else {
                                var size = data.data[i].images.standard_resolution.url;
                            }
                            $("#portfolio").append("<div class='portfolio-item osteoin col-md-4 col-sm-6'><img src='" + size + "' alt='tagosteoin' /><a target='_blank' href='" + data.data[i].link + "'><img src='images/plus.png' alt='portfolio-arrow' /></a><div class='portfolio-item-hover'> <h3>" + data.data[i].caption.text + "</h3><h4>@osteoin</h4></div></div>");
                            contador -= 1;
                        }
                    }
                }
            });
        }
    }
}();

$(document).ready(function() {
    instagramTag.init();
    instagramPerfil.init();
});