<?php

        require '../php/includes/phpmailer/PHPMailerAutoload.php';

        //Create a new PHPMailer instance
        $mail = new PHPMailer;

        // Set PHPMailer to use the sendmail transport
        $mail->isSendmail();

        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $nome		= trim($_REQUEST['contact-name']);	// Pega o valor do campo Nome
        $email		= trim($_REQUEST['contact-email']);	// Pega o valor do campo Email
        $tel		= trim($_REQUEST['contact-phone']);	// Pega o valor do campo Telefone
        $mensagem	= trim($_REQUEST['contact-message']);	// Pega os valores do campo Mensagem

        //Set who the message is to be sent from
        $mail->setFrom($email, $nome);

        //Set who the message is to be sent to
        $mail->addAddress('contato@osteoin.com.br', 'Osteoin');

        //Set the subject line
        $mail->Subject = 'Contato WebSite Osteoin';


        $body = '<html><body>Nome: '.$nome.'<br>Telefone: '.$tel.'<br>Email: '.$email.'<br>Mensagem: '.$mensagem.'<br></body></html>';

        $mail->msgHTML($body);

        //Replace the plain text body with one created manually
        $mail->AltBody = 'This is a plain-text message body';

        //send the message, check for errors
        $resultado = null;
        if (!$mail->send()) {
            $resultado = false;
        } else {
            $resultado = true;
        }